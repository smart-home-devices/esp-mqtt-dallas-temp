#include <OneWire.h>
#include <DallasTemperature.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// #define NEED_SERIAL_PRINT

// Dallas configuration
OneWire oneWire(2);
DallasTemperature dallasSensors(&oneWire);

// Deep sleep configuration
constexpr uint64_t deepSleepTime = 30 * 1000 * 1000; // mcs

// Wifi configuration
constexpr const char * const ssid = "Your Wifi Network";
constexpr const char * const password = "Your Wifi Password";
constexpr uint8_t maxWifiReconnectAttempts = 20;

// Mqtt configuration
constexpr const char * const server = "Your Mqtt Server Ip (Hostname)";
constexpr uint16_t port = 1883;
constexpr const char * const clientName = "Your Device's Client Name";
constexpr const char * const topic = "/sensor/temp/floor1";
constexpr uint8_t maxMqttReconnectAttempts = 3;

WiFiClient wifiClient;
PubSubClient client(server, port, wifiClient);

bool wifiConnect()
{
  #ifdef NEED_SERIAL_PRINT
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);
  #endif

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  uint8_t attempts = maxWifiReconnectAttempts;
  while (WiFi.status() != WL_CONNECTED && attempts > 0)
  {
    --attempts;
    delay(500);
    #ifdef NEED_SERIAL_PRINT
      Serial.print(".");
    #endif
  }

  if (WiFi.status() != WL_CONNECTED)
  {
    return false;
  }

  #ifdef NEED_SERIAL_PRINT
    Serial.println();
    Serial.print("WiFi connected.");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
  #endif

  return true;
}

bool mqttConnect()
{
  uint8_t attempts = maxMqttReconnectAttempts;
  
  while (!client.connected() && attempts > 0)
  {
    --attempts;

    #ifdef NEED_SERIAL_PRINT
      Serial.print("Attempting MQTT connection...");
    #endif
    
    if (client.connect(clientName))
    {
      #ifdef NEED_SERIAL_PRINT
        Serial.println("connected");
      #endif
    }
    else
    {
      #ifdef NEED_SERIAL_PRINT
        Serial.print("failed, rc=");
        Serial.print(client.state());
        Serial.println(" try again in 5 seconds");
      #endif
      delay(5000);
    }
  }
  
  return client.connected();
}

bool mqttEmit(String topic, String value)
{
  #ifdef NEED_SERIAL_PRINT
    Serial.print("Sending value ");
    Serial.print(value);
    Serial.print(" to topic ");
    Serial.print(topic);
    Serial.print("... Status: ");
  #endif
  
  bool ret = client.publish(topic.c_str(), value.c_str());
  
  #ifdef NEED_SERIAL_PRINT
    Serial.println(ret);
  #endif
  
  return ret;
}

bool mqttEmit(String topic, float value)
{
  char s[8];
  dtostrf(value, 7, 2, s);
  s[7] = 0;
  return mqttEmit(topic, String(s));
}

void setup()
{
  dallasSensors.begin();

  #ifdef NEED_SERIAL_PRINT
    Serial.begin(115200);
  #endif

  if (wifiConnect() && mqttConnect())
  {
    dallasSensors.requestTemperatures();
    delay(750); // to dallas collect data
    mqttEmit(topic, dallasSensors.getTempCByIndex(0));
    delay(1000); // to success publish
  }

  #ifdef NEED_SERIAL_PRINT
    Serial.print("Deep sleep for ");
    Serial.print(deepSleepTime / 1000.0 / 1000.0);
    Serial.println(" s");
  #endif
  ESP.deepSleep(deepSleepTime);
}

void loop()
{
}
